# windflow-examples.

Implementation of some [DSPBench](https://github.com/GMAP/DSPBench/)
applications using the WindFlow framework.

Some applications require third party libraries to function properly, which
are mentioned in the respective README file.

All applications rely on the [nlohmann JSON
library](https://github.com/nlohmann/json) to produce JSON files contianing
performance measurements.
